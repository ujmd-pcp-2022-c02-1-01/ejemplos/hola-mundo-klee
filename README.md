# Hola Mundo Klee



Hola mundo personalizado en Klee un personaje de Genshi Impact, este hola mundo contiene un menu lateral izquierdo, que contiene dos botones, el botón 1 que cumplirá la función de saludar, será el: hola mundo. Por el contrario, también tendrá un botón 2, el cual indicará la despedida, nombrado: adiós mundo. Aparte de cumplir esa función, en la parte derecha de la aplicación se mostrará una imagen dependiendo de la opción seleccionada. Esta aplicaión de hola mundo esta diseñada en Visual Studio Community como una aplicación de formularios para Windows con .NET Framework.
